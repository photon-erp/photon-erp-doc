# Regras de Negócio

<!--
## RN0000 - XXXXX XXXXX XXXX

* **Identificador**: Sufixo seguido de um identificador único. O sufixo geralmente utilizado é RN (Regra de Negócio) e o identificador único geralmente é composto de quatro dígitos (podendo ser mais, conforme a o tamanho do sistema que está sendo especificado).
* **Nome**: Nome curto da RN, mas que possibilite entender bem o que RN faz apenas pelo nome.
* **Módulo**: Módulo ao qual o RF pertence. Se for um sistema pequeno que não possua nenhum módulo, somente o próprio sistema, deve ser preenchido com N/A (não se aplica).
* **Data de criação**: Data da criação da RN, ou a data em que ela foi especificada.
* **Autor**: Profissional que especificou a RN pela primeira vez, quem a criou.
* **Data da última alteração**: Data em que houve a última alteração no RN.
* **Autor**: Profissional que alterou a especificação da RN pela última vez.
* **Versão**: Número da versão do RN. Geralmente utiliza-se algo simples, como 1, 2 etc. A versão inicial sempre é a 1, e a cada alteração incrementa-se a versão (na criação versão 1, na primeira alteração versão 2 etc.).
* **Dependências**: Quais RFs (Requisitos Funcionais) são dependentes da RN para serem realizados. Coloca-se apenas o identificador dos RFs.
* **Descrição**: Descrição detalhada (a mais detalhada possível) da RN.

Template:

## RN0000 - XXXXX XXXXX XXXXX

* **Identificador**:
* **Nome**:
* **Módulo**:
* **Data de criação**:
* **Autor**:
* **Data da última alteração**:
* **Autor**:
* **Versão**:
* **Dependências**:
* **Descrição**:
-->

## RN0001 - Valor de venda do produto

* **Identificador**: RN0001
* **Nome**: Valor de venda do produto
* **Módulo**:
* **Data de criação**: 04/03/2022
* **Autor**: Guilherme Peres
* **Data da última alteração**: N/A
* **Autor**: N/A
* **Versão**: 1
* **Dependências**: RF0003 - Gerenciar Venda
* **Descrição**: Cada cliente possuí uma tabela com os valores cobrados por produto. Devido a cada cliente possuir um desconto financeiro e prazo de pagamento diferente.
Os clientes podem possuir um CD (Centro de Distribuição) e ou PTP (Ponto a Ponto), o valor do produto difere de acordo com a unidade faturada, devido a porcentagem do desconto financeiro aplicado.

---

## RN0002 - Venda de produtos por lote

* **Identificador**: RN0002
* **Nome**: Venda de produtos por lote
* **Módulo**:
* **Data de criação**: 04/03/2022
* **Autor**: Guilherme Peres
* **Data da última alteração**: N/A
* **Autor**: N/A
* **Versão**: 1
* **Dependências**: RF0003 - Gerenciar Venda
* **Descrição**: Um produto pode ser vendido mesmo que seja de lotes diferentes, desde o mesmo esteja hábil para venda (prazo de validade disponível para atender o cliente).

---

## RN0003 - Cálculo de comissão do representante comercial

* **Identificador**: RN0003
* **Nome**: Cálculo de comissão do representante comercial
* **Módulo**:
* **Data de criação**: 04/03/2022
* **Autor**: Guilherme Peres
* **Data da última alteração**: N/A
* **Autor**: N/A
* **Versão**: 1
* **Dependências**: RF0005 - Gerenciar Caixa
* **Descrição**: O representante comercial é comissionado sobre as vendas realizadas, tendo como porcentagem das vendas dos produtos por unidade faturada.