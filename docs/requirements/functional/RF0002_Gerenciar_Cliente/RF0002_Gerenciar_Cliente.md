# RF0002 - Gerenciar Pessoa

## História de Usuário

Eu como administrador desejo gerenciar os personagens que interagem com o sistema. Sendo eles:

* Pessoa Física
* Pessoa Jurídica
* Cliente
* Fornecedor
* Funcionário

Caso o cliente seja do tipo `Pessoa Jurídica`, o mesmo poderá cadastrar outra(s) pessoa(s) do tipo `Pessoa Física` que terão acesso e representarão a empresa vinculada ao seu cadastro.

## Diagrama de Caso de Uso

![Diagrama de Caso de Uso](../../../diagrams/out/software/requirements/functional/RF0002%20-%20Gerenciar%20Cliente/diagrama_classe.svg)

## Caso de Uso de Alto Nível

## Caso de Uso Expandido

## Modelo Conceitual

## Diagrama de Sequência

## Contrato

## Diagrama de Sequência