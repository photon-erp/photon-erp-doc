<!--
# RF000X - XXXXX XXXXX

!!! cite "História de Usuário"

## Diagrama de Caso de Uso

* CU000X - XXXXX XXXXX
* CU000X - XXXXX XXXXX
* CU000X - XXXXX XXXXX
* CU000X - XXXXX XXXXX

## CU000X - XXXXX XXXXX

### Caso de Uso (Alto Nível)

* **Nome**: CU000X - XXXXX XXXXX
* **Tipo**: Essencial / Desejável
* **Ator(res)**:
* **Descrição**:

### Caso de Uso (Expandido)

* **Nome**:
* **Tipo**:
* **Ator(res)**:
* **Responsabilidade**:
* **Pré-condição**:
* **Pós-condição**:

Fluxo

| Ações do Ator | Respostas do Sistema |
| ------------- | -------------------- |
| 1.            | 2.                   |
| 3.            | 4.                   |

Exceções

| Nº Item | Descrição |
| ------- | --------- |

### Diagrama de Sequência

### Contrato

#### Seta X

* **Nome**:
* **Responsabilidade**:
* **Exceção**:
* **Saída**:
* **Pré-Condição**:
* **Pós-Condição**:
-->

# RF0001 - Gerenciar Usuário

!!! cite "História de Usuário"

    Eu como administrador desejo gerenciar os usuários que interagem com o sistema. Sendo eles:

    * Administrador
    * Representante Comercial

## Diagrama de Caso de Uso

![Diagrama de Caso de Uso](../../../diagrams/out/software/requirements/functional/RF0001%20-%20Gerenciar%20Usuário/diagrama_caso_uso.svg)

* CU0001 - Cadastrar Usuário
* CU0002 - Pesquisar Usuário
* CU0003 - Editar Usuário
* CU0004 - Desativar Usuário
* CU0005 - Ativar Usuário
* CU0006 - Excluir Usuário

## CU0001 - Cadastrar Usuário

### Caso de Uso (Alto Nível)

* **Nome**: CU0001 - Cadastrar Usuário
* **Tipo**: Essencial
* **Ator(res)**: Administrador
* **Descrição**: Acessa a área de usuários; Seleciona a opção de inserir usuário; Preenche o formulário e confirma

### Caso de Uso (Expandido)

* **Nome**: CU0001 - Cadastrar Usuário
* **Tipo**: Essencial
* **Ator(res)**: Administrador
* **Responsabilidade**: Inserir um novo usuário no sistema
* **Pré-condição**: Ator logado
* **Pós-condição**: Novo usuário cadastrado no sistema

Fluxo

| Ações do Ator                                       | Respostas do Sistema                                |
| --------------------------------------------------- | --------------------------------------------------- |
| 1. Seleciona a opção Usuários                       | 2. Exibe a tela de Usuários                         |
| 3. Seleciona a opção de inserir usuário             | 4. Exibe o formulário para inserção de novo usuário |
| 5. Preenche formulário com os dados do novo usuário | 6. Valida os dados preenchidos no formulário                     |
| 7. Confirma inserção de novo usuário                | 8. Exibe mensagem de confirmação de inserção na tela de usuários |

Exceções

| Nº Item | Descrição                                                                                      |
| ------- | ---------------------------------------------------------------------------------------------- |
| 2       | Ator tem a sessão expirada; Sistema direciona para área de login                               |
| 6       | Dado obrigatório não preenchido ou preenchido incorretamente; Sistema exibe mensagem de alerta |

<!--
### Diagrama de Sequência

![Diagrama de Sequência](../../../../diagrams/out/software/requirements/functional/RF0001%20-%20Gerenciar%20Usuário/diagrama_sequencia.svg)
### Contrato

#### Seta X

* **Nome**:
* **Responsabilidade**:
* **Exceção**:
* **Saída**:
* **Pré-Condição**:
* **Pós-Condição**:

### Cenário de Teste
-->

### Casos de Teste


[Caso de Uso](#cu0001---cadastrar-usuário)