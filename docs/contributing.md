# Contribuição

As seguintes instruções fornecerão uma cópia do projeto em funcionamento em sua máquina local para fins de desenvolvimento e teste, as instruções estão agrupadas para a execução de cada repositório.

## Desenvolvimento

### API

Se você já clonou o repositório: [massas-universo-api](https://github.com/massas-universo/massas-universo-api) e, você sabe que precisa se aprofundar no código, aqui estão algumas diretrizes para configurar seu ambiente.

#### Pré-requisitos

É necessário que tenha instalado os seguintes itens:

* [Python 3.8+](https://docs.python.org/3/)
* [Docker](https://docs.docker.com/get-started/)
* [Docker Compose](https://docs.docker.com/compose/)

#### Ambiente virtual com `venv`

Você pode criar um ambiente virtual em um diretório utilizando o módulo `venv` do Python. O seguinte comando criará um diretório `./venv/` com os binários do Python e então você poderá instalar pacotes para este ambiente isolado.

```shell
python -m venv venv
```

#### Ativar o ambiente virtual

Ative o ambiente virtual com:

=== "Linux, macOS, Windows Bash"

    ```shell
    source ./venv/bin/activate
    ```

=== "Windows PowerShell"

    ```console
    .\venv\Scripts\Activate.ps1
    ```

Para verificar se funcionou, use:

=== "Linux, macOS, Windows Bash"

    ```shell
    which pip
    ```

=== "Windows PowerShell"

    ```console
    Get-Command pip
    ```

Se for exibido o binário `pip` em `dirrepositorio/venv/bin/pip`, então funcionou!

!!! tip "Dica"

    Toda vez que você instalar um novo pacote com pip nesse ambiente, ative o ambiente novamente.

    Isso garante que, se você usar um programa de terminal instalado por esse pacote, use aquele do seu ambiente local e não qualquer outro que possa ser instalado globalmente.

#### Instalação das dependências

No diretório raiz do projeto e com o ambiente virtual ativado, instale as dependências utilizando o pip:

```
pip install -r requirements.txt
```

#### Criação das variáveis de ambiente

Como boa prática todas as informações sensíveis para a execução do projetos foram abstraídas para variáveis de ambiente. Na raiz do projeto crie um arquivo de nome `.env`, o mesmo deve conter os seguintes parâmetros:

```title=".env"
API_HOST=0.0.0.0
API_PORT=8000
DATABASE_ENGINE=django.db.backends.postgresql
DATABASE_HOST=db
DATABASE_NAME=devmassasuniversodb
DATABASE_PASSWORD=devmassasunivesodbpassword
DATABASE_PORT=5432
DATABASE_USER=devmassasuniversodbuser
DEBUG=True
SECRET_KEY=supersecuresecretkey
```

As variáveis são utilizadas entre: Django, Docker Compose e Postgreql

#### Execução do ambiente via docker-compose

Você pode iniciar os containers via docker-compose, com o seguinte comando:

```shell
docker-compose up
```

!!! info "Makefile"

    O projeto contém um Makefile que possibilita a chamada de alias criados para simplificar os comandos, consulte o mesmo!

Após iniciar os containers você já pode consultar a aplicação através do host e porta configurado no arquivo `.env`. Neste exemplo a mesma esta acessível em: <http://0.0.0.0:8000>

#### Execução dos testes

#### Construção

Esta API segue o padrão Rest, é utilizado as seguintes tecnologias:

![Django](https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=green)
![Django Rest Framework](https://img.shields.io/badge/django%20rest-ff1709?style=for-the-badge&logo=django&logoColor=white)
![Python](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=blue)

## Documentação