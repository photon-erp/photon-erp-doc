# ℹ️ Sobre

## 🔢 Versionamento

Utilizamos [SemVer](https://semver.org/) para versões. Para as versões disponíveis, veja [tags neste repositório](https://github.com/sts-br/massas-universo/tags).

## 👥 Autores
<!--TODO: Adicionar Haniel como contribuidor-->

[Guilherme Alves Peres](https://allmylinks.com/guialvesp1)

## 📃 Licença
<!--TODO: Definir e configurar licença do projeto-->

Este projeto está licenciado sob a licença GNU General Public License v3.0 - veja o arquivo [LICENSE](LICENSE) para obter mais detalhes.