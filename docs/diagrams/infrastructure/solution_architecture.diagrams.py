from diagrams import Cluster, Diagram
from diagrams.onprem.container import Docker
from diagrams.onprem.database import Postgresql
from diagrams.onprem.vcs import Github
from diagrams.aws.compute import EC2ContainerRegistry
from diagrams.aws.storage import S3
from diagrams.programming.framework import Django


with Diagram("Solution Architecture", show=False):

    with Cluster("Developer Environment"):
        api_vcs = Github("massas-universo-api")
        with Cluster("Container: API"):
            api = Django("API")
        with Cluster("Container: DB"):
            db = Postgresql("Database")
        with Cluster ("Container: Proxy (NGNIX)"):
            proxy = Docker("NGNIX")


    bucket = S3("massas-universo-api")
    proxy >> api
    proxy << api
    proxy >> bucket
    proxy << bucket
    api >> db
    api << db
    api_container_registry = EC2ContainerRegistry("AWS Container Registry")
    api_vcs >> api_container_registry
